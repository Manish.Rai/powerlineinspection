# Some of the screenshots of the output from the V1.0 of this project

The model can predict 4 classes:
* Defective Conductors
* Defective Insulators
* Insulators
* Rusts in fitting.

**Note:** Not all the outputs have the _true positive_ and _true negative_ achievements. Some of them results _false positive_ and _false negative_ that can be improve in later versions.

![output1](./images/output1.png)

![output2](./images/output2.png)

![output3](./images/output3.png)

![output4](./images/output4.png)

![output5](./images/output5.png)

![output6](./images/output6.png)

![output7](./images/output7.png)

![output8](./images/output8.png)

![output9](./images/output9.png)

![output10](./images/output10.png)

![output11](./images/output11.png)

![output12](./images/output12.png)

![output13](./images/output13.png)

![output14](./images/output14.png)

![output15](./images/output15.png)

![output16](./images/output16.png)

![output17](./images/output17.png)

![output18](./images/output18.png)

![output19](./images/output19.png)

![output20](./images/output20.png)
