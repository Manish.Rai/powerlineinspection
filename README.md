# PowerLine Inspection with AI/ML using Drone

Development of PoC for the powerline inspection with AI/ML using Drone which is a collaborative project between DHI(Innotech), BPC and Garuda Robotics. The AI model built in this project will have the ability to predict if the the Powerline Component has any sort of defect. 

## V1.0
As of now the model can predict four classes of defects:
* Defective conductor
![defective_conductor](./Sample_images/IMG_048888.jpg)
* Defective Insulator
![defective_insulator](./Sample_images/IMG_0453.jpg) 
* Rusts
![Rusts](./Sample_images/IMG_0491.jpg)
* Insulators
![insulator](./Sample_images/IMG.jpg)


Click [here](https://gitlab.com/Manish.Rai/powerlineinspection/-/blob/main/GR_BPC_outputs/Outputs.md) to check out some of the outputs from this V1.0 of the project.

The AI model was train with around 250 images for all the four classes. The images were taken from Simtokha Substation BPC. 

The backbone model used to train the images for powerline inspection is SSD MobileNet V2 FPN Lite which is available from [Tensorflow Model Zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md).

[Checkout](https://github.com/tzutalin/labelImg) the annotation tool used to annotate the image data for this project.